package dark.robot.calculator.geometric;

import dark.robot.calculator.geometric.figure.Circle;
import dark.robot.calculator.geometric.figure.Figures;
import dark.robot.calculator.geometric.figure.Rectangle;
import dark.robot.calculator.geometric.figure.Square;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class GeometricCalculatorTest {

    Map<String, Double> arguments;
    private IGeometricCalculator calculator;

    @Before
    public void setUp() throws Exception {
        calculator = GeometricCalculator.getInstance();
        arguments = new HashMap<>();
    }

    @Test
    public void testCalculateCircleAreaWithRadio() throws Exception {
        Double radio = 4.0;
        Double expected = 50.26548245743669;

        arguments.put(Circle.Keys.radio.name(), radio);
        Double actual = calculator.calculateArea(Figures.Circle.name(), arguments);

        assertEquals(expected, actual);
    }

    @Test
    public void testCalculateCircleAreaWithDiameter() throws Exception {
        Double diameter = 6.0;
        Double expected = 28.274333882308138;

        arguments.put(Circle.Keys.diameter.name(), diameter);
        Double actual = calculator.calculateArea(Figures.Circle.name(), arguments);

        assertEquals(expected, actual);
    }

    @Test
    public void testCalculateCirclePerimeterWithRadio() throws Exception {
        Double radio = 4.0;
        Double expected = 25.132741228718345;

        arguments.put(Circle.Keys.radio.name(), radio);
        Double actual = calculator.calculatePerimeter(Figures.Circle.name(), arguments);

        assertEquals(expected, actual);
    }

    @Test
    public void testCalculateCirclePerimeterWithDiameter() throws Exception {
        Double diameter = 4.0;
        Double expected = 12.566370614359172;

        arguments.put(Circle.Keys.diameter.name(), diameter);
        Double actual = calculator.calculatePerimeter(Figures.Circle.name(), arguments);

        assertEquals(expected, actual);
    }

    @Test
    public void testCalculateRectangleArea() throws Exception {
        Double longSide = 3.0;
        Double shortSide = 2.0;
        Double expected = 6.0;

        arguments.put(Rectangle.Keys.sideA.name(), longSide);
        arguments.put(Rectangle.Keys.sideB.name(), shortSide);
        Double actual = calculator.calculateArea(Figures.Rectangle.name(), arguments);

        assertEquals(expected, actual);
    }

    @Test
    public void testCalculateRectanglePerimeter() throws Exception {
        Double longSide = 3.0;
        Double shortSide = 2.0;
        Double expected = 10.0;

        arguments.put(Rectangle.Keys.sideA.name(), longSide);
        arguments.put(Rectangle.Keys.sideB.name(), shortSide);
        Double actual = calculator.calculatePerimeter(Figures.Rectangle.name(), arguments);

        assertEquals(expected, actual);
    }

    @Test
    public void testCalculateSquareAreaWithSide() throws Exception {
        Double side = 10.0;
        Double expected = 100.0;

        arguments.put(Square.Keys.side.name(), side);
        Double actual = calculator.calculateArea(Figures.Square.name(), arguments);

        assertEquals(expected, actual);
    }

    @Test
    public void testCalculateSquareAreaWithDiagonal() throws Exception {
        Double diagonal = 10.0;
        Double expected = 50.0;

        arguments.put(Square.Keys.diagonal.name(), diagonal);
        Double actual = calculator.calculateArea(Figures.Square.name(), arguments);

        assertEquals(expected, actual);
    }

    @Test
    public void testCalculateSquarePerimeterWithSide() throws Exception {
        Double side = 4.0;
        Double expected = 16.0;

        arguments.put(Square.Keys.side.name(), side);
        Double actual = calculator.calculatePerimeter(Figures.Square.name(), arguments);

        assertEquals(expected, actual);
    }

    @Test
    public void testCalculateSquarePerimeterWithDiagonal() throws Exception {
        Double diagonal = 4.0;
        Double expected = 11.313708498984761;

        arguments.put(Square.Keys.diagonal.name(), diagonal);
        Double actual = calculator.calculatePerimeter(Figures.Square.name(), arguments);

        assertEquals(expected, actual);
    }
}