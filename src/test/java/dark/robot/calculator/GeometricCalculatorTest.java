package dark.robot.calculator;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/geometric-calculator.feature", format = "pretty")
public class GeometricCalculatorTest {
}