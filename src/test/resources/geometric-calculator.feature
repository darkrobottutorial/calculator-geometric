Feature: Calculate area and perimeter 

  Scenario: Calculate a circle area using radio
    Given the "4.0" radio
    When use the geometric calculator to get a circle area using circle radio
    Then the circle area is "50.26548245743669"

  Scenario: Calculate a circle area using diameter
    Given the "6.0" diameter
    When use the geometric calculator to get a circle area using circle diameter
    Then the circle area is "28.274333882308138"

  Scenario: Calculate a circle perimeter using radio
    Given the "4.0" radio
    When use the geometric calculator to get a circle perimeter using circle radio
    Then the circle perimeter is "25.132741228718345"

  Scenario: Calculate a circle perimeter using diameter
    Given the "4.0" diameter
    When use the geometric calculator to get a circle perimeter using circle diameter
    Then the circle perimeter is "12.566370614359172"

  Scenario: Calculate a rectangle area
    Given the "3.0" and "2.0" sides
    When use the geometric calculator to get a rectangle area
    Then the rectangle area is "6.0"

  Scenario: Calculate a rectangle perimeter
    Given the "3.0" and "2.0" sides
    When use the geometric calculator to get a rectangle perimeter
    Then the rectangle perimeter is "10.0"

  Scenario: Calculate a square area using side
    Given the "10.0" sides
    When use the geometric calculator to get a square area using side
    Then the square area is "100.0"

  Scenario: Calculate a square area using diagonal
    Given the "10.0" diagonal
    When use the geometric calculator to get a square area using diagonal
    Then the square area is "50.0"

  Scenario: Calculate a square perimeter using side
    Given the "4.0" sides
    When use the geometric calculator to get a square perimeter using side
    Then the square perimeter is "16.0"

  Scenario: Calculate a square perimeter using diagonal
    Given the "4.0" diagonal
    When use the geometric calculator to get a square perimeter using diagonal
    Then the square perimeter is "11.313708498984761"