package dark.robot.calculator.geometric;

import java.util.Map;

public interface IGeometricCalculator {
    Double calculateArea(String figureType, Map<String, Double> arguments);

    Double calculatePerimeter(String figureType, Map<String, Double> arguments);
}