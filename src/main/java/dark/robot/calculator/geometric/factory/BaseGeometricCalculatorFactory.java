package dark.robot.calculator.geometric.factory;

public interface BaseGeometricCalculatorFactory {
    IFigure createFigure(String type);
}