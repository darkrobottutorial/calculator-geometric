package dark.robot.calculator.geometric.figure;

public enum Figures {
    Square,
    Rectangle,
    Circle;

    public static Figures get(String type) {
        for (Figures figure : Figures.values()) {
            if (figure.name().toLowerCase().equals(type.toLowerCase())) {
                return figure;
            }
        }
        throw new FigureNotExistsException(String.format(FigureNotExistsException.DEFAULT_MESSAGE, type));
    }
}