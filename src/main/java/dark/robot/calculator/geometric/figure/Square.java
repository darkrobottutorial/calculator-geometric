package dark.robot.calculator.geometric.figure;

import dark.robot.calculator.basic.BasicCalculator;
import dark.robot.calculator.basic.ICalculator;
import dark.robot.calculator.geometric.factory.IFigure;
import dark.robot.validator.IValidator;
import dark.robot.validator.Validator;

import java.util.Map;

public class Square implements IFigure {

    private static Square square = null;
    private IValidator validator;
    private ICalculator calculator;
    private Square() {
        validator = Validator.getInstance();
        calculator = BasicCalculator.getInstance();
    }

    public static Square getInstance() {
        Square instance = square;

        if (instance == null) {
            instance = new Square();
        }

        return instance;
    }

    @Override
    public Double getArea(Map<String, Double> arguments) {
        validator.validate(arguments);
        Double area;

        if (arguments.containsKey(Keys.side.name())) {
            Double side = arguments.get(Keys.side.name());
            area = Math.pow(side, 2);
        } else if (arguments.containsKey(Keys.diagonal.name())) {
            Double diameter = arguments.get(Keys.diagonal.name());
            area = calculator.divide(Math.pow(diameter, 2.0), 2.0);
        } else {
            throw new IllegalArgumentException();
        }

        return area;
    }

    @Override
    public Double getPerimeter(Map<String, Double> arguments) {
        validator.validate(arguments);
        Double perimeter;

        if (arguments.containsKey(Keys.side.name())) {
            Double side = arguments.get(Keys.side.name());
            perimeter = calculator.multiply(4.0, side);
        } else if (arguments.containsKey(Keys.diagonal.name())) {
            Double diameter = arguments.get(Keys.diagonal.name());
            perimeter = calculator.multiply(calculator.multiply(2.0, Math.sqrt(2)), diameter);
        } else {
            throw new IllegalArgumentException();
        }

        return perimeter;
    }

    public enum Keys {
        side,
        diagonal
    }
}