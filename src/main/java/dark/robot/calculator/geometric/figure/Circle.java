package dark.robot.calculator.geometric.figure;

import dark.robot.calculator.basic.BasicCalculator;
import dark.robot.calculator.basic.ICalculator;
import dark.robot.calculator.geometric.factory.IFigure;
import dark.robot.validator.IValidator;
import dark.robot.validator.Validator;

import java.util.Map;

public class Circle implements IFigure {

    private static Circle circle = null;
    private IValidator validator;
    private ICalculator calculator;
    private Circle() {
        validator = Validator.getInstance();
        calculator = BasicCalculator.getInstance();
    }

    public static Circle getInstance() {
        Circle instance = circle;

        if (instance == null) {
            instance = new Circle();
        }

        return instance;
    }

    @Override
    public Double getArea(Map<String, Double> arguments) {
        validator.validate(arguments);
        Double area;

        if (arguments.containsKey(Keys.radio.name())) {
            Double radio = arguments.get(Keys.radio.name());
            area = calculator.multiply(Math.PI, Math.pow(radio, 2));
        } else if (arguments.containsKey(Keys.diameter.name())) {
            Double diameter = arguments.get(Keys.diameter.name());
            area = calculator.multiply(Math.PI, calculator.divide(Math.pow(diameter, 2.0), 4.0));
        } else {
            throw new IllegalArgumentException();
        }

        return area;
    }

    @Override
    public Double getPerimeter(Map<String, Double> arguments) {
        validator.validate(arguments);
        Double perimeter;

        if (arguments.containsKey(Keys.radio.name())) {
            Double radio = arguments.get(Keys.radio.name());
            perimeter = calculator.multiply(2.0, calculator.multiply(Math.PI, radio));
        } else if (arguments.containsKey(Keys.diameter.name())) {
            Double diameter = arguments.get(Keys.diameter.name());
            perimeter = calculator.multiply(Math.PI, diameter);
        } else {
            throw new IllegalArgumentException();
        }

        return perimeter;
    }

    public enum Keys {
        radio,
        diameter
    }
}