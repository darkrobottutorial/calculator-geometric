package dark.robot.calculator.geometric.figure;

import dark.robot.calculator.basic.BasicCalculator;
import dark.robot.calculator.basic.ICalculator;
import dark.robot.calculator.geometric.factory.IFigure;
import dark.robot.validator.IValidator;
import dark.robot.validator.Validator;

import java.util.Map;

public class Rectangle implements IFigure {

    private static Rectangle rectangle = null;
    private IValidator validator;
    private ICalculator calculator;
    private Rectangle() {
        validator = Validator.getInstance();
        calculator = BasicCalculator.getInstance();
    }

    public static Rectangle getInstance() {
        Rectangle instance = rectangle;

        if (instance == null) {
            instance = new Rectangle();
        }

        return instance;
    }

    @Override
    public Double getArea(Map<String, Double> arguments) {
        validator.validate(arguments);

        Double longSide = arguments.get(Keys.sideA.name());
        Double shortSide = arguments.get(Keys.sideB.name());

        validator.validate(longSide, shortSide);

        return calculator.multiply(longSide, shortSide);
    }

    @Override
    public Double getPerimeter(Map<String, Double> arguments) {
        validator.validate(arguments);

        Double longSide = arguments.get(Keys.sideA.name());
        Double shortSide = arguments.get(Keys.sideB.name());

        validator.validate(longSide, shortSide);

        return calculator.multiply(2.0, calculator.add(longSide, shortSide));
    }

    public enum Keys {
        sideA,
        sideB
    }
}